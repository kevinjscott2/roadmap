angular.module('roadmap').directive 'fullMilestone', [
  '$rootScope', '$location', '$filter', '$timeout', 'label', 'milestones', 'subitems', 'user', 'timeline', 'roadmap',
  ($rootScope, $location, $filter, $timeout, label, milestones, subitems, user, timeline, roadmap) ->
    magickURL = (url) ->
      $location.path url
      $location.replace()
      history.pushState null, '', url

    link = (scope) ->
      scope.show = false
      scope.new_record = false
      scope.isMultiline = false

      sheet = (->
        style = document.createElement 'style'
        style.appendChild document.createTextNode ''
        document.head.appendChild style

        style.sheet
      )()

      setColor = ->
        color = label.getColor(scope.milestone.label_id)
        sheet.addRule 'td.ui-datepicker-current-day', 'background-color: ' + color + ' !important'

      urlName = (name) ->
        name.toLowerCase().replace(/[&\/\\#,+()$~%.'":*?<>{}\[\]\n]/g, '').split(' ').join('-').substring(0, 64)

      workingDaysBetweenDates = (startDate, endDate) ->
        if endDate < startDate
          t = endDate
          endDate = startDate
          startDate = t

        millisecondsPerDay = 86400 * 1000
        startDate.setHours 0, 0, 0, 1
        endDate.setHours 23, 59, 59, 999
        diff = endDate - startDate
        days = Math.ceil(diff / millisecondsPerDay)

        weeks = Math.floor(days / 7)
        days = days - (weeks * 2)

        startDay = startDate.getDay()
        endDay = endDate.getDay()

        days = days - 2 if startDay - endDay > 1

        days = days - 1 if startDay == 0 && endDay != 6

        days = days - 1 if endDay == 6 && startDay != 0

        days

      scope.dateDiff = ->
        workingDaysBetweenDates(new Date(), new Date(scope.milestone.date)) - 1

      scope.direction = ->
        if new Date() < new Date(scope.milestone.date)
          'away'
        else
          'ago'

      scope.eq = ->
        today = new Date()
        later = new Date(scope.milestone.date)

        today.setHours 0, 0, 0, 0
        later.setHours 0, 0, 0, 0

        today.valueOf() == later.valueOf()

      scope.update = ->
        unless scope.new_record
          milestones.update {
            id: scope.milestone.id
            token: user.getToken()
            description: scope.milestone.description
            timeline_id: scope.milestone.timeline_id
            label_id: scope.milestone.label_id
            date_end: $filter('date')(scope.milestone.date, 'yyyy-MM-dd')
          },
            ->
              $rootScope.$broadcast 'notify', {
                type: 'success'
                text: 'Milestone saved'
              }
              scope.labelsDropdown = false
              scope.datepicker = false

              $rootScope.$broadcast 'updated'
            ->
              $rootScope.$broadcast 'notify', {
                type: 'failure'
                text: 'Unable to save milestone'
              }
              scope.labelsDropdown = false

      scope.save = ->
        return unless scope.canSaveMilestone()

        scope.milestone.date_end = $filter('date')(scope.milestone.date, 'yyyy-MM-dd')
        milestones.save {
          description: scope.milestone.description
          timeline_id: scope.milestone.timeline_id
          label_id: scope.milestone.label_id
          date_end: scope.milestone.date_end
        },
          (success) ->
            $rootScope.$broadcast 'notify', {
              type: 'success'
              text: 'Milestone saved'
            }

            scope.milestone.id = success.id
            timeline.addMilestone scope.milestone, ->
              $rootScope.$broadcast 'archived'
              scope.show = false

            $rootScope.$broadcast 'updated'

            angular.forEach scope.milestone.sub_items, (subitem) ->
              subitems.save {
                token: user.getToken()
                milestone_id: scope.milestone.id
                name: subitem.name
              }, (success) ->
                subitem.id = success.id
          ->
            $rootScope.$broadcast 'notify', {
              type: 'failure'
              text: 'Unable to save milestone'
            }

      scope.bgc = (id) ->
        id = scope.milestone.label_id unless id
        {
          backgroundColor: label.getColor(id)
        }

      scope.label = (id) ->
        id = scope.milestone.label_id unless id
        label.getName(id)

      scope.labels = -> label.all()

      scope.hideMilestone = ->
        scope.$broadcast 'milestone:hide'

        roadmap.go timeline.getRoadmap().id, true

        scope.show = false
        scope.labelsDropdown = false
        scope.datepicker = false

      scope.setLabel = (id) ->
        scope.milestone.label_id = id
        scope.update()

        setColor()

      scope.signedin = -> user.signedin()

      scope.remove = ->
        removeMilestone = confirm 'Are you sure to remove milestone?'

        if removeMilestone
          timeline.removeMilestone scope.milestone.id, ->
            $rootScope.$broadcast 'archived'
            $rootScope.$broadcast 'notify', {
              type: 'success'
              text: 'Milestone removed'
            }

            scope.show = false

      scope.addSubitem = (subitem) ->
        console.log '[Full milestone] subitem adding'

        if subitem and subitem.length > 0
          if scope.new_record
            scope.milestone.sub_items = [] unless scope.milestone.sub_items
            scope.milestone.sub_items.push {name: subitem}
            scope.subitem = ''
          else
            newSubitem =  {name: subitem}
            scope.milestone.sub_items.push newSubitem

            subitems.save {
              token: user.getToken()
              milestone_id: scope.milestone.id
              name: subitem
            }, (success) ->
              newSubitem.id = success.id
              $rootScope.$broadcast 'notify', {
                type: 'success'
                text: 'Subitem added'
              }

            scope.subitem = ''

          scope.$broadcast 'editable', 'new-todo'

      scope.updateSubitem = (subitemIndex) ->
        (name)->
          unless scope.new_record
            subitems.update {
              token: user.getToken()
              id: scope.milestone.sub_items[subitemIndex].id
              name: name
            }, ->
              $rootScope.$broadcast 'notify', {
                type: 'success'
                text: 'Subitem updated'
              }

      scope.removeSubitem = (subitem) ->
        scope.milestone.sub_items.splice subitem, 1
        unless scope.new_record
          subitems.delete {
            token: user.getToken()
            id: scope.milestone.sub_items[subitem].id
          }, ->
            $rootScope.$broadcast 'notify', {
              type: 'success'
              text: 'Subitem removed'
            }

      scope.canSaveMilestone = ->
        scope.milestone.date and scope.milestone.label_id and scope.milestone.description.length > 0

      scope.$watch 'milestone.date_end', (new_value, old_value) ->
        tmpDate = new Date new_value
        scope.milestone.date = tmpDate.valueOf()

        if new_value and old_value
          scope.update()

      scope.sortableOptions =
        axis: 'y'

      scope.menuClose = ->
        scope.$broadcast 'menu:close'

      scope.$watch 'milestone.sub_items', (new_value, old_value) ->
        orderEqual = (oldValue, newValue)->
          result = true
          _.each oldValue, (subItem, index)->
            result = false if subItem.id != newValue[index]?.id
          result

        if old_value and new_value and new_value.length == old_value.length and !orderEqual(old_value, new_value)
          console.log '[Full milestone] subitems reordering'

          subitems.sort {
            id: scope.milestone.id
            sorted_ids: _.pluck(scope.milestone.sub_items, 'id').join ','
          }, ->
            _.each scope.milestone.sub_items, (item, index) ->
              item.position = index
      , true

      scope.$watch 'milestone.description', ->
        index = scope.milestone.description.indexOf '\n'
        scope.milestone.isMultiline = (index > -1 and index < scope.milestone.description.length - 1)

      scope.$on 'milestone:show', (evt, new_record) ->
        console.log '[Full milestone] milestone:show catched'

        scope.milestone = timeline.getActiveMilestone()

        if scope.milestone
          scope.new_record = new_record

          scope.show = true

          unless new_record
            magickURL '/m/' + scope.milestone.hash + '/' + urlName(scope.milestone.description)

          if new_record
            $timeout ->
              scope.$broadcast 'editable', 'description'
            , 300

          setColor()

      scope.$on 'menu:close', ->
        scope.labelsDropdown = false
        scope.datepicker = false

      scope.$on 'milestone:close', -> scope.hideMilestone()

      scope.$on 'labels:open', -> scope.labelsDropdown = true if scope.show
      scope.$on 'labels:close', -> scope.labelsDropdown = false if scope.show

      scope.$on 'labels:set', (evt, labelId) ->
        if labelId >= 0 and labelId < label.all().length
          scope.setLabel label.all()[labelId].id

      scope.$on 'saving', -> scope.save() if scope.new_record

      scope.formatCalendar = {numberOfMonths: [4, 1]}

    {
      restrict: 'E'
      replace: true
      templateUrl: 'full_milestone.html'
      link: link
    }
]
