angular.module('infiniteScroll', []).directive 'infiniteScroll', ->
  {
    link: (scope, element, attrs) ->
      offset = parseInt(attrs.threshold) || 0
      e = element[0]

      element.bind 'mousewheel', ->
        height = Math.max document.body.offsetHeight, window.innerHeight

        if scope.$eval(attrs.canLoad) and height - document.body.offsetTop - window.innerHeight <= -offset
          scope.$apply attrs.infiniteScroll
  }
