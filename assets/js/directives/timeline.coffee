angular.module('roadmap').directive 'timeline', [
  '$rootScope', 'timelines', 'timeline', 'label', 'user',
  ($rootScope, timelines, timeline, label, user) ->
    link = (scope) ->
      scope.menu = false
      scope.editMode = false

      scope.update = ->
        timelines.update {
          id: scope.timeline.id,
          token: user.getToken(),
          title: scope.timeline.title,
          is_archive: scope.timeline.is_archive
          roadmap_id: scope.timeline.roadmap_id
        }, ->
          $rootScope.$broadcast 'notify', {
            type: 'info'
            text: 'Timeline updated'
          }

      scope.toggleMenu = ->
        scope.menu = !scope.menu
        $rootScope.$broadcast 'menu:close' if !scope.menu

      scope.archive = ->
        timeline.archive scope.timeline.id, ->
          $rootScope.$broadcast 'archived'
          $rootScope.$broadcast 'notify', {
            type: 'info'
            text: 'Timeline archived'
          }

      scope.addMilestone = ->
        timeline.newMilestone scope.timeline.id
        $rootScope.$broadcast 'milestone:show', true

      scope.copyMove = ->
        scope.menu = false
        $rootScope.$broadcast 'timeline:move', scope.timeline.id

      scope.$on 'edit:mode', (evt, editMode) -> scope.editMode = editMode

      scope.$on 'menu:close', ->
        scope.menu = false
        scope.copyDialog = false

      $rootScope.$broadcast 'edit'

    {
      restrict: 'E',
      replace: true,
      templateUrl: 'timeline.html',
      link: link,
      scope: {
        timeline: '='
      }
    }
]
