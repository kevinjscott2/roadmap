angular.module('roadmap').directive 'editable', [
  '$rootScope', '$timeout', 'user',
  ($rootScope, $timeout, user) ->
    link = (scope, element) ->
      scope.edit_mode = false
      scope.old_value = scope.value
      scope.helpDropdown = false

      cancel = ->
        unless scope.editId == 'sub-item-edit'
          scope.value = scope.old_value

        scope.edit_mode = false

      scope.setEditMode = ->
        $rootScope.$broadcast 'menu:close'

        if user.signedin()
          scope.edit_mode = true
          scope.old_value = scope.value

          $timeout ->
            if scope.big
              element.find('textarea')[0].focus()
            else
              element.find('input')[0].focus()
          , 0

      scope.save = ->
        $timeout ->
          unless scope.helpClicked
            scope.edit_mode = false
            scope.callback()(scope.value)

            scope.old_value = scope.value
            if scope.reset
              scope.value = ''
              scope.old_value = ''
          scope.helpClicked = false
        , 100

      scope.displayValue = ->
        if scope.value
          scope.value
        else
          scope.placeholder

      scope.toggleHelpDropdown = ->
        scope.helpClicked = true

        scope.helpDropdown = !scope.helpDropdown

      scope.$on 'menu:close', ->
        scope.helpDropdown = false
        cancel()

      scope.$on 'editable', (evt, id) ->
        scope.setEditMode() if id == scope.editId

      element.on 'keydown', (e) ->
        e.stopPropagation() if e.keyCode != 13 and e.keyCode != 17

        scope.$apply -> cancel() if e.keyCode == 27

      element.on 'blur', ->
        scope.$apply -> scope.save()

    {
      restrict: 'EA'
      templateUrl: 'editable.html'
      link: link
      scope: {
        value: '='
        callback: '&'
        placeholder: '='
        reset: '='
        editId: '='
        big: '='
      }
    }
]
