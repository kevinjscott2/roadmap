angular.module('roadmap').directive 'shortcuts', ['$rootScope', ($rootScope) ->
  link = (scope) ->
    scope.showHelp = false

    scope.shortcuts = [
      {
        title: 'Close menu / Cancel editing'
        key: 'esc'
        description: 'Cancel or close the thing being edited'
      }
      {
        title: 'Toggle edit mode'
        key: 'e'
      }
      {
        title: 'Open filter menu'
        key: 'f'
      }
      {
        title: 'Clear all filters'
        key: 'x'
      }
      {
        title: 'Label'
        key: 'l'
        description: 'Pressing "l" opens a pop-over of the available labels. Clicking a label will add or remove it from the milestone. Pressing one of the following number keys, will apply or remove that label.'
      }
      {
        title: 'Add new timeline'
        key: 't'
      }
      {
        title: 'Add new milestone to most recently hovered timeline'
        key: 'm'
      }
      {
        title: 'Help'
        key: '?'
        description: 'Show this dialog'
      }
    ]

    scope.$on 'help:toggle', -> scope.showHelp = !scope.showHelp
    scope.$on 'menu:close', -> scope.showHelp = false

  {
    restrict: 'E'
    replace: true
    templateUrl: 'shortcuts.html'
    link: link
  }
]
