angular.module('roadmap').directive 'milestone', [
  '$rootScope', 'label', 'timeline',
  ($rootScope, label, timeline) ->
    link = (scope, element, attr) ->
      scope.editMode = true
      scope.menu = false

      workingDaysBetweenDates = (startDate, endDate) ->
        if endDate < startDate
          t = endDate
          endDate = startDate
          startDate = t

        millisecondsPerDay = 86400 * 1000
        startDate.setHours 0, 0, 0, 1
        endDate.setHours 23, 59, 59, 999
        diff = endDate - startDate
        days = Math.ceil(diff / millisecondsPerDay)

        weeks = Math.floor(days / 7)
        days = days - (weeks * 2)

        startDay = startDate.getDay()
        endDay = endDate.getDay()

        days = days - 2 if startDay - endDay > 1

        days = days - 1 if startDay == 0 && endDay != 6

        days = days - 1 if endDay == 6 && startDay != 0

        days

      scope.toggleMenu = ->
        scope.menu = !scope.menu

      scope.bgc = ->
        {
          backgroundColor: label.getColor(scope.milestone.label_id)
        }

      scope.dateDiff = ->
        workingDaysBetweenDates(new Date(), new Date(scope.milestone.date)) - 1

      scope.direction = ->
        if new Date() < new Date(scope.milestone.date)
          'away'
        else
          'ago'

      scope.eq = ->
        today = new Date()
        later = new Date(scope.milestone.date)

        today.setHours 0, 0, 0, 0
        later.setHours 0, 0, 0, 0

        today.valueOf() == later.valueOf()

      scope.overflowed = -> element.scrollHeight > 200

      scope.showMilestone = ->
        timeline.setMilestone scope.milestone
        $rootScope.$broadcast 'milestone:show', false

    {
      restrict: 'E'
      replace: true
      templateUrl: 'milestone.html'
      link: link
      scope: {
        milestone: '='
      }
    }
]
