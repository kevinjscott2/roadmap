angular.module('roadmap').directive 'markdown', ->
  converter = new Showdown.converter()

  {
    restrict: 'EA'
    scope: {
      mdValue: '='
    }
    link: (scope, element, attrs) ->
      scope.$watch 'mdValue', ->
        element.html converter.makeHtml scope.mdValue
  }
