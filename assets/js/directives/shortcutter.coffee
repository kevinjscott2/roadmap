angular.module('roadmap').directive 'shortcutter', [
  '$rootScope', 'ability',
  ($rootScope, ability) ->
    link = (scope, element) ->
      fullMilestone = false
      editMode = false
      closeLabels = false
      labelsPressed = false

      ctrlPressed = false
      shiftPressed = false

      element.on 'keydown', (e) ->
        scope.$apply ->
          switch e.keyCode
            when 13
              console.log '[shortcutter] saving (enter)'

              $rootScope.$broadcast 'saving' if ctrlPressed
            when 16
              console.log '[shortcutter] (shift)'

              shiftPressed = true
            when 17
              console.log '[shortcutter] saving (ctrl)'

              ctrlPressed = true
            when 27
              console.log '[shortcutter] esc'

              if fullMilestone
                $rootScope.$broadcast 'milestone:close'
              else
                $rootScope.$broadcast 'menu:close'
            when 69
              console.log '[shortcutter] (e)dit mode'

              if ability.ableTo 'roadmap:manage'
                editMode = !editMode
                $rootScope.$broadcast 'edit:mode', editMode
            when 70
              console.log '[shortcutter] (f)ilters'

              editMode = !editMode
              $rootScope.$broadcast 'filters:show'
            when 76
              console.log '[shortcutter] (l)abels'

              $rootScope.$broadcast 'labels:open'
              closeLabels = false
              labelsPressed = true
            when 77
              console.log '[shortcutter] (m)ilestone'

              $rootScope.$broadcast 'milestone:new'
            when 84
              console.log '[shortcutter] (t)imeline'

              $rootScope.$broadcast 'timeline:new'
            when 88
              console.log '[shortcutter] (x) reset filters'

              $rootScope.$broadcast 'filters:reset'
            when 191
              console.log '[shortcutter] toggle help (?)'

              $rootScope.$broadcast 'help:toggle'
            else
              console.log '[shortcutter] another key: '

          if labelsPressed
            $rootScope.$broadcast 'labels:set', e.keyCode - 49 if 49 <= e.keyCode <= 57
            $rootScope.$broadcast 'labels:set', e.keyCode - 97 if 97 <= e.keyCode <= 105

      element.on 'keyup', (e) ->
        scope.$apply ->
          switch e.keyCode
            when 17
              console.log '[shortcutter] saving (ctrl)'

              ctrlPressed = false
            when 76
              console.log '[shortcutter] (l)abels'

              labelsPressed = false
              $rootScope.$broadcast 'labels:close' if closeLabels
            else
              console.log '[shortcutter] another key'

      scope.$on 'milestone:show', -> fullMilestone = true
      scope.$on 'milestone:hide', -> fullMilestone = false

      scope.$on 'edit:mode', (evt, mode) -> editMode = mode

    {
      restrict: 'A'
      link: link
    }
]
