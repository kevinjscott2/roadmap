angular.module('keitoaino', []).directive('stopEvent', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.on(attr.stopEvent, function (e) {
                if (e.keyCode != 27) e.stopPropagation();
            });
        }
    };
});
