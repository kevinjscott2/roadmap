angular.module('roadmap').controller 'sbLabelsCtrl', [
  '$scope', '$rootScope', 'label', 'labels',
  ($scope, $rootScope, label, labels) ->
    getLabels = ->
      $scope.labels = label.all()

    $scope.bgc = (id) ->
      {
        backgroundColor: label.getColor id
      }

    $scope.updateLabels = ->
      angular.forEach $scope.labels, (l) ->
        labels.update {
          id: l.id
          roadmap_id: l.roadmap_id
          text: l.text
        }, ->
          $rootScope.$broadcast 'notify', {
            text: 'Labels updated'
            type: 'info'
          }

    $scope.$on 'saving', -> $scope.updateLabels()

    getLabels()
]
