angular.module('roadmap').controller 'sbArchivedTimelinesCtrl', [
  '$scope', '$rootScope', 'timeline',
  ($scope, $rootScope, timeline) ->
    getTimelines = ->
      timeline.archived (success) ->
        $scope.timelines = success

    $scope.remove = (tl) ->
      timeline.remove tl.id, ->
        getTimelines()
        $rootScope.$broadcast 'notify', {
          type: 'info'
          text: 'Timeline removed permanently'
        }

    $scope.restore = (tl) ->
      timeline.restore tl.id, ->
        getTimelines()
        $rootScope.$broadcast 'updated'
        $rootScope.$broadcast 'notify', {
          type: 'success'
          text: 'Timeline restored'
        }

    $scope.show = (tl) ->
      timeline.select tl
      $scope.go 4

    $scope.$on 'archived', -> getTimelines()

    getTimelines()
]
