angular.module('roadmap').controller 'roadmapCtrl', [
  '$scope', '$rootScope', '$filter', '$routeParams', '$location', '$timeout', '$browser', 'roadmap', 'timeline', 'label', 'filter', 'ability', 'user', 'activity', 'socketIO',
  ($scope, $rootScope, $filter, $routeParams, $location, $timeout, $browser, roadmap, timeline, label, filter, ability, user, activity, socketIO) ->
    $scope.is_roadmap = true

    $scope.roadmap = null

    $scope.timelines = {}
    $scope.dates = {}

    $scope.full = false

    $scope.subscriptionDropdown = false
    $scope.roadmapTitleDropdown = false
    $scope.privacyDropdown = false

    $scope.newTimeline = null

    $scope.infinite = {
      page: 0,
      load: true
    }

    $scope.recentTimeline = null

    $scope.copyDialog = false

    loadActivities = -> activity.all $scope.roadmap.id, (result) -> $scope.activities = result

    loadData = ->
      postPostProcessing = ->
        console.log '[Roadmap:controller] postPostProcessing'

        # get timelines and milestones
        timeline.active (success) -> $scope.timelines = success
        timeline.dates false, (success) ->
          $scope.dates = success
          $timeout ->
            $('.timeline').mCustomScrollbar 'scrollTo', [-$('#old-milestones').height(), 0]
          , 1000

        $scope.roadmap = timeline.getRoadmap()

        loadActivities()

        socketIO.on 'connect', ->
          socketIO.emit 'action', {type: 'subscribe', modelType: 'Roadmap', modelId: $scope.roadmap.id}

      postProcessing = ->
        # set abilities
        ability.set $scope.roadmap.rights, user.getUser().role == 1

        # set filters
        if $location.search().f
          filter.set $location.search().f
          $location.search ''

        # set current roadmap and load labels
        timeline.setRoadmap $scope.roadmap, -> label.load $scope.roadmap.id

        postPostProcessing()

      if $routeParams.roadmap_hash
        console.log '[Roadmap:controller] roadmap hash present'

        # load roadmap
        roadmap.findByHash $routeParams.roadmap_hash,
          (success) ->
            $scope.roadmap = success

            # go to full roadmap url or to root
            if $scope.roadmap
              roadmap.go $scope.roadmap.id
            else
              $location.path '/'

            postProcessing()
          ->
            $rootScope.$broadcast 'notify', {
              text: 'Roadmap not found'
              type: 'failure'
            }
            $timeout ->
              $location.path '/'
            , 1000

      if $routeParams.milestone_hash
        console.log '[Roadmap:controller] milestone hash present'

        if timeline.getActiveMilestone()
          console.log '[Roadmap:controller] active milestone found'

          postPostProcessing()
          $rootScope.$broadcast 'milestone:show', false

          console.log '[Roadmap:controller] milestone:show broadcasted'
        else
          console.log '[Roadmap:controller] active milestone not found'

          # load roadmap by milestone hash
          roadmap.findByMilestone $routeParams.milestone_hash, (result) ->
            $scope.roadmap = result

            # go to full roadmap url
            unless $scope.roadmap
              roadmap.go $scope.roadmap.id

            postProcessing()

    $scope.visible = ->
      result = []

      angular.forEach $scope.timelines, (timeline) ->
        result.push timeline if timeline.visible

      result

    $scope.filtered = (milestones, date) ->
      result = []

      angular.forEach milestones, (milestone) ->
        if milestone.date == parseInt(date) and label.visible(milestone.label_id)
          result.push milestone

      result

    $scope.hideMilestone = ->
      $scope.full = false

    $scope.closeRoadmap = ->
      roadmap.close $scope.roadmap.id, (success) ->
        if success
          $rootScope.$broadcast 'notify', {
            text: 'Roadmap archived'
            type: 'info'
          }
        else
          $rootScope.$broadcast 'notify', {
            text: 'Unable to archive roadmap'
            type: 'error'
          }

    $scope.exportRoadmap = ->
      roadmap.export $scope.roadmap.hash

    $scope.leaveRoadmap = ->
      roadmap.leave $scope.roadmap.id,
        ->
          $location.path '/'
        ,
        ->
          $rootScope.$broadcast 'notify', {
            text: 'Unable to leave roadmap'
            type: 'failure'
          }

    $scope.updateRoadmap = ->
      roadmap.update $scope.roadmap, ->
        $rootScope.$broadcast 'notify', {
          text: 'Roadmap updated'
          type: 'success'
        }
        $scope.roadmapTitleDropdown = false

    contentWidth = ->
      byTimelines = 500 * Object.keys($scope.timelines).length
      byTimelines += 500 if $scope.editMode and $scope.can('roadmap:manage')

      Math.max(byTimelines, window.innerWidth)

    $scope.tlStyle = ->
      {
        width: contentWidth() + 'px'
        maxWidth: contentWidth() + 'px'
      }

    $scope.privacyLabel = ->
      if $scope.roadmap.is_private
        'Private'
      else
        'Public'

    $scope.setPrivacy = (privacy) ->
      roadmap.setPrivacy $scope.roadmap.id, privacy, ->
        $rootScope.$broadcast 'notify', {
          text: 'Privacy settings changed'
          type: 'info'
        }

    $scope.setRecentTimeline = (timelineId) -> $scope.recentTimeline = timelineId

    $scope.copyMoveTimeline = (action, roadmapId) ->
      if action == 'copy'
        timeline.copyTo $scope.copyMoveTimelineId, roadmapId,
          ->
            $scope.copyDialog = false
            if roadmapId == $scope.roadmap.id
              timeline.forceLoad (success) -> $scope.timelines = success

            $rootScope.$broadcast 'notify', {
              text: 'Timeline copied'
              type: 'success'
            }
          ,
          ->
            $rootScope.$broadcast 'notify', {
              text: 'Unable to copy timeline'
              type: 'failure'
            }

      if action == 'move'
        timeline.moveTo $scope.copyMoveTimelineId, roadmapId,
          ->
            $scope.copyDialog = false

            $rootScope.$broadcast 'notify', {
              text: 'Timeline moved'
              type: 'success'
            }
          ,
          ->
            $rootScope.$broadcast 'notify', {
              text: 'Unable to move timeline'
              type: 'failure'
            }

    $scope.showFilters = -> $rootScope.$broadcast 'filters:show'

    $scope.toggleSubscriptionDropdown = -> $scope.subscriptionDropdown = !$scope.subscriptionDropdown
    $scope.toggleEditRoadmapTitle = -> $scope.roadmapTitleDropdown = !$scope.roadmapTitleDropdown
    $scope.togglePrivacyDropdown = -> $scope.privacyDropdown = !$scope.privacyDropdown

    $scope.closeDropdown = ->
      $scope.subscriptionDropdown = false
      $scope.roadmapTitleDropdown = false
      $scope.privacyDropdown = false

    $scope.unsubscribe = -> roadmap.subscription $scope.roadmap.id, false

    getYesterdayDate = ->
      date = new Date()
      date.setHours(0)
      date.setMinutes(0)
      date.setSeconds(0)
      date.setMilliseconds(0)

      date.getTime() - 1

    $scope.oldDates = ->
      oldDates = {}
      yesterday = getYesterdayDate()

      angular.forEach $scope.dates, (v, date) ->
        oldDates[date] = v if date <= yesterday

      oldDates

    $scope.currentDates = ->
      currentDates = {}
      yesterday = getYesterdayDate()

      angular.forEach $scope.dates, (v, date) ->
        currentDates[date] = v if date > yesterday

      currentDates

    $scope.addTimeline = ->
      return unless $scope.newTimeline

      $scope.newTimeline = $scope.newTimeline.trim()

      timeline.addTimeline $scope.newTimeline, ->
        $rootScope.$broadcast 'notify', {
          text: 'Timeline added'
          type: 'info'
        }
        $rootScope.$broadcast 'updated'
        $rootScope.$broadcast 'menu:close'

    $scope.activitiesStyle = ->
      activitiesTitle = document.getElementById 'activities-title'
      height = window.innerHeight - activitiesTitle.offsetTop - 100
      {
        height: height + 'px'
      }

    $scope.$on 'sign:in', ->
      $rootScope.$broadcast 'edit:mode', true

    onUpdate = ->
      timeline.active (success) ->
        $scope.timelines = success

      timeline.dates false, (success) ->
        $scope.dates = success

    $scope.$on 'archived', onUpdate
    $scope.$on 'updated', onUpdate

    $scope.$on 'milestones:loaded', ->
      console.log '[Roadmap:controller] showing milestone'

      targetMilestone = null
      angular.forEach $scope.timelines, (timeline) ->
        angular.forEach timeline.milestones, (milestone) ->
          if milestone.hash == $routeParams.milestone_hash
            targetMilestone = milestone
      timeline.setMilestone targetMilestone

      $rootScope.$broadcast 'milestone:show', false

    $scope.$on 'milestone:new', ->
      if $scope.recentTimeline and $scope.editMode
        timeline.newMilestone $scope.recentTimeline
        $rootScope.$broadcast 'milestone:show', true

    $scope.$on 'timeline:move', (evt, timelineId) ->
      console.log '[Roadmap:controller] copy/move timeline'

      $scope.copyMoveTimelineId = timelineId

      $scope.copyDialog = true
      $scope.copyMove = 'copy'

      roadmap.all (success) ->
        $scope.roadmaps = success
        $scope.selectedRoadmap = $scope.roadmaps[0]

    $scope.$on 'menu:close', ->
      $scope.copyDialog = false
      $scope.subscriptionDropdown = false
      $scope.roadmapTitleDropdown = false

    $scope.$on 'update:activity', -> loadActivities()

    $scope.$on 'sign:in', -> loadData()

    makeUI = ->
      container = $('.timeline')

      container.mCustomScrollbar {
        axis: 'xy'
        scrollInertia: 0
        contentWidth: contentWidth(),
        callbacks: {
          whileScrolling: ->
            if navigator.userAgent.toLowerCase().indexOf('firefox') > -1
              $('.titles').css {
                top: -this.mcs.top + 'px'
              }
        }
        advanced: {
          updateOnContentResize: true
        }
      }

      $('.mCSB_container').draggable {
        start: ->
          container.mCustomScrollbar 'stop'
        drag: (evt, ui) ->
          ui.position.left = 0 if ui.position.left > 0
          ui.position.top = 0 if ui.position.top > 0

          if navigator.userAgent.toLowerCase().indexOf('firefox') > -1
            $('.titles').css {
              top: -ui.position.top + 'px'
            }
        stop: (evt, ui) ->
          ui.position.left = 0 if ui.position.left > 0
          ui.position.top = 0 if ui.position.top > 0
          container.mCustomScrollbar 'update'
      }

    $(window).load -> makeUI()

    $scope.$watch 'timelines', ->
      makeUI()
    , true


    loadData()
    $rootScope.$broadcast 'edit:mode', true if user.isAdmin() or $scope.can('roadmap:manage')
    $scope.showSidebar() if $scope.editMode and $routeParams.roadmap_hash
]
