angular.module('roadmap').controller 'usersCtrl', [
  '$scope', '$rootScope', '$timeout', 'roadmap_users',
  ($scope, $rootScope, $timeout, roadmap_users) ->
    pages = ['users', 'user', 'new_user']
    page = 0

    $scope.users = []

    $scope.go = (index) -> page = index
    $scope.page = -> pages[page] + '.html'

    $scope.viewUser = (user) ->
      $scope.user = user

      $scope.go 1

    $scope.addUser = ->
      $scope.newUser = {
        email: ''
        role: 2
      }

      $scope.go 2

      $timeout ->
        angular.element(document.getElementById('new-user-email')).focus()
      , 500

    $scope.userImage = (user) ->
      {
        backgroundImage: 'url(' + user.avatar + ')'
      }

    $scope.addUserComplete = ->
      roadmap_users.add $scope.newUser,
        (success) ->
          $rootScope.$broadcast 'notify', {
            type: 'success'
            text: 'User added'
          }
          $scope.viewUser success
        ->
          $rootScope.$broadcast 'notify', {
            type: 'failure'
            text: 'User with this email already exists'
          }

    $scope.setRole = (user, role_id) ->
      roadmap_users.setRole user, role_id, ->
        $rootScope.$broadcast 'notify', {
          type: 'success'
          text: 'Role changed'
        }

    $scope.remove = (user) ->
      if confirm 'Are you sure to remove this user?'
        roadmap_users.remove user,
          ->
            $rootScope.$broadcast 'notify', {
              type: 'success'
              text: 'User removed'
            }
            $scope.go 0
          ->
            $rootScope.$broadcast 'notify', {
              type: 'failure'
              text: 'Unable to remove user'
            }


    roadmap_users.all (success) ->
      $scope.users = success
]
