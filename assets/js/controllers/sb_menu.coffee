angular.module('roadmap').controller 'sbMenuCtrl', [
  '$scope', '$rootScope', 'member', 'user', 'users', 'roadmap',
  ($scope, $rootScope, member, user, users, roadmap) ->
    pages = ['sb_menu', 'archived_timelines', 'labels', 'filters', 'archived_timeline']
    page = 0
    prev = 0

    $scope.members = []
    $scope.dropdown = [false, false]
    $scope.menu = false
    $scope.searching = false

    $scope.new_member = ''

    memberPages = ['member', 'permissions']
    memberPage = 0

    loadMembers = (roadmap_id) ->
      unless roadmap_id
        roadmap_id = $scope.roadmap.id if $scope.roadmap

      if roadmap_id
        member.all roadmap_id, (success) ->
          $scope.members = success

    addMember = (id) ->
      member.add id,
      ->
        $rootScope.$broadcast 'notify', {
          text: 'Member successfully added'
          type: 'success'
        }

        loadMembers()
        $scope.dropdown = [false, false]
      ->
        $rootScope.$broadcast 'notify', {
          text: 'Unable to add member'
          type: 'failure'
        }

    $scope.go = (index) ->
      prev = page
      page = index

    $scope.back = ->
      # change to normal stacked back later...
      if page != prev
        page = prev
      else
        page = 0

    $scope.page = -> pages[page] + '.html'

    $scope.toggleDropdown = ->
      $scope.dropdown[0] = false
      $scope.dropdown[1] = !$scope.dropdown[1]

    $scope.showMember = (member) ->
      $scope.member = member

      $scope.dropdown[0] = true
      $scope.dropdown[1] = false

      memberPage = 0

    $scope.addMember = (newMember) ->
      unless newMember
        if $scope.users and $scope.users.length > 0
          newMember = $scope.users[0]
        else
          newMember = $scope.new_member.trim()

      return unless newMember

      if typeof newMember == 'string'
        users.save {
          email: newMember
        },
          (success) ->
            addMember success.id
          ->
            $rootScope.$broadcast 'notify', {
              text: 'Unable to add member'
              type: 'failure'
            }
      else
        addMember newMember.id

    $scope.revokeAccess = ->
      member.revokeAccess $scope.member.id, ->
        loadMembers()
        $scope.dropdown = [false, false]

    $scope.toggleMenu = -> $scope.menu = !$scope.menu

    $scope.closeDropdown = -> $scope.dropdown = [false, false]

    $scope.rights = (rights) ->
      switch rights
        when 0
          'Admin'
        else
          'Normal'

    $scope.toggleSubscription = ->
      roadmap.subscription $scope.roadmap.id, !$scope.roadmap.is_subscribed

    $scope.subscriptionMenuTitle = ->
      if $scope.roadmap.is_subscribed
        'Subscribed'
      else
        'Subscribe'

    $scope.memberAvatar = (user) ->
      if user
        {
          backgroundImage: 'url(' + user.avatar + ')'
        }

    $scope.$on 'menu:close', ->
      $scope.dropdown = [false, false]

    $scope.$watch 'new_member', ->
      if $scope.new_member.trim().length > 0
        $scope.searching = true
        users.search {
          email: $scope.new_member
        }, (success) ->
          $scope.users = success
      else
        $scope.searching = false

  #  change permissions
    $scope.memberPage = -> memberPages[memberPage] + '.html'

    $scope.changePermissions = -> memberPage = 1

    $scope.setPermissions = (selectedMember, rights) ->
      return if selectedMember.rights == rights
      member.setPermissions selectedMember, rights,
        ->
          $rootScope.$broadcast 'notify', {
            text: 'Rights successfully changed'
            type: 'success'
          }

          $scope.dropdown[0] = false
          selectedMember.rights = rights
        ->
          $rootScope.$broadcast 'notify', {
            text: 'Unable to change rights'
            type: 'failure'
          }

    $scope.openFilters = (open) ->
      if open
        $scope.go 3
        $rootScope.$broadcast 'reset'

    $scope.$on 'filters:show', ->
      $scope.go 3
      $rootScope.$broadcast 'reset'

    $scope.$on 'members:update', (evt, roadmap_id) ->
      console.log 'members:update'
      loadMembers roadmap_id

    loadMembers()
]
