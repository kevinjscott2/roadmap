angular.module('roadmap').controller 'profileCtrl', [
  '$scope', '$rootScope', '$location', 'user', 'subscription', 'roadmap', '$upload', 'roadmap_users',
  ($scope, $rootScope, $location, user, subscription, roadmap, $upload, roadmap_users) ->
    $scope.tabs = ['subscriptions', 'settings']
    $scope.currentTab = 0

    $scope.subscriptions = []
    $scope.user = user.getUser()

    $scope.changeAvatar = false

    subscribe = (targetSubscription) ->
      subscription.subscribe targetSubscription.roadmap_id, ->
        targetSubscription.is_subscribed = true

    unsubscribe = (targetSubscription) ->
      subscription.unsubscribe targetSubscription.roadmap_id, ->
        targetSubscription.is_subscribed = false

    $scope.switchTab = (index) -> $scope.currentTab = index
    $scope.tab = -> $scope.tabs[$scope.currentTab] + '.html'

    $scope.toggleSubscription = (targetSubscription) ->
      if targetSubscription.is_subscribed
        unsubscribe targetSubscription
      else
        subscribe targetSubscription

    $scope.subscriptionLabel = (subscription) ->
      if subscription.is_subscribed
        'unsubscribe'
      else
        'subscribe'

    $scope.updateSettings = ->
      user.updateEmail ->
        $rootScope.$broadcast 'notify', {
          text: 'Setting saved'
          type: 'success'
        }

    $scope.userAvatar = ->
      {
        backgroundImage: 'url(' + $scope.user.image + ')'
      }

    $scope.onImageSelect = ($files) ->
      $upload.upload({
        url: API_HOST + 'users/upload_avatar'
        data: {
          token: user.getToken()
          avatar: $files[0]
        }
      }).success (response) ->
        user.setImage response.avatar

        $rootScope.$broadcast 'menu:close'

    $scope.toggleAvatarChange = -> $scope.changeAvatar = !$scope.changeAvatar

    $scope.removeSelf = ->
      if confirm 'Are you sure to remove your account? (this change cannot be undone)'
        roadmap_users.remove user.getUser(),
          ->
            user.reset()
            $location.path '/'
          ->
            $rootScope.$broadcast 'notify', {
              type: 'failure'
              text: 'Unable to remove account'
            }

    $scope.$on 'menu:close', -> $scope.changeAvatar = false

    subscription.all (success) ->
      angular.forEach success, (subscription) ->
        if subscription.is_subscribed
          roadmap.findById subscription.roadmap_id, (foundRoadmap) ->
            if foundRoadmap
              $scope.subscriptions.push {
                roadmap_id: subscription.roadmap_id
                roadmap_name: foundRoadmap.roadmap.name
                is_subscribed: true
              }
]
