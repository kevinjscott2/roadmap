angular.module('roadmap').controller 'sbfiltersCtrl', [
  '$scope', '$rootScope', '$location', 'timeline', 'label',
  ($scope, $rootScope, $location, timeline, label) ->
    getTimelines = ->
      timeline.active (success) ->
        $scope.timelines = success

    getLabels = ->
      $scope.labels = label.all()

    updateHash = ->
      timelines = []
      labels = []

      angular.forEach $scope.timelines, (timeline) ->
        timelines.push timeline.id if timeline.visible
      angular.forEach $scope.labels, (label) ->
        labels.push label.id if label.visible

      hashids = new Hashids('roadmap-frontend')

      $location.search {f: hashids.encrypt(timelines) + ',' + hashids.encrypt(labels)}

    $scope.labelColor = (l) ->
      {
        backgroundColor: label.getColor l.id
      }

    $scope.toggleTimeline = (timeline) ->
      timeline.visible = !timeline.visible
      $rootScope.$broadcast 'updated'

      updateHash()

    $scope.toggleLabel = (label) ->
      label.visible = !label.visible
      $rootScope.$broadcast 'updated'

      updateHash()

    $scope.reset = ->
      getTimelines()
      getLabels()

      angular.forEach $scope.timelines, (timeline) ->
        timeline.visible = true

      angular.forEach $scope.labels, (label) ->
        label.visible = false

      $rootScope.$broadcast 'updated'

    $scope.isFiltered = ->
      filtered = false

      angular.forEach $scope.timelines, (timeline) ->
        filtered = true unless timeline.visible

      angular.forEach $scope.labels, (label) ->
        filtered = true if label.visible

      filtered

    $scope.$on 'filters:reset', -> $scope.reset()

    getTimelines()
    getLabels()
]
