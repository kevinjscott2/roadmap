angular.module('roadmap').controller 'mainCtrl', [
  '$scope', '$rootScope', '$timeout', '$location', 'user', 'ability',
  ($scope, $rootScope, $timeout, $location, user, ability) ->
    $scope.notification = {
      title: '',
      class: ''
    }
    $scope.editMode = false
    $scope.sidebar = false
    $scope.dropdown = false
    $scope.roadmapsDropdown = false
    $scope.usersDropdown = false

    $scope.addIndex = null
    $scope.filters = false

    $scope.isRoadmap = false

    $scope.closeAll = -> $rootScope.$broadcast 'menu:close'

    $scope.toggleEditMode = ->
      if $scope.editMode
        $rootScope.$broadcast 'edit:mode', false
      else
        if user.signedin()
          $rootScope.$broadcast 'edit:mode', true

    $scope.showSidebar = -> $scope.sidebar = true
    $scope.hideSidebar = -> $scope.sidebar = false

    $scope.toggleDropdown = ->
      oldState = $scope.dropdown

      $rootScope.$broadcast 'menu:close'
      $scope.dropdown = !oldState

    $scope.toggleRoadmaps = ->
      $scope.roadmapsDropdown = !$scope.roadmapsDropdown

      $timeout ->
        angular.element(document.getElementById('find-roadmap')).focus() if $scope.roadmapsDropdown
      , 500

    $scope.toggleUsers = ->
      $scope.usersDropdown = !$scope.usersDropdown

      $timeout ->
        angular.element(document.getElementById('find-user')).focus() if $scope.usersDropdown
      , 500

    $scope.user = -> user.getUser()

    $scope.editButtonTitle = ->
      if $scope.editMode
        'done editing'
      else
        'edit roadmap'

    $scope.can = (action, resource) -> ability.ableTo action, resource
    $scope.cannot = (action, resource) -> !$scope.can action, resource

    $scope.signin = (provider) -> user.signin provider
    $scope.signed = -> user.signedin()

    $scope.viewProfile = -> $location.path '/profile'
    $scope.homeScreen = -> $location.path '/'

    $scope.setGithubToken = (token) ->
      if token.length > 20
        user.signinGithubToken token

    $scope.userAvatar = -> {backgroundImage: 'url(' + $scope.user().image + ')'}

    $scope.admin = -> user.isAdmin()

    $scope.$on 'notify', (evt, data) ->
      $scope.notification.title = data.text
      $scope.notification.class = data.type + ' active'

      $timeout ->
        $scope.notification.class = data.type
      , 3000

    $scope.$on 'sign:in', -> $rootScope.$broadcast 'edit:mode', true

    $scope.$on 'edit:mode', (evt, editMode) ->
      $scope.editMode = editMode

    $scope.$on 'menu:close', ->
      $scope.dropdown = false
      $scope.roadmapsDropdown = false
      $scope.usersDropdown = false

    $scope.$on 'edit', ->
      sidebar_state = $scope.sidebar
      $rootScope.$broadcast 'edit:mode', $scope.editMode
      $scope.sidebar = sidebar_state

    $scope.$on 'logout', -> user.logout()

    $scope.$on 'request', (evt, state) -> $scope.spinner = state

    $scope.$on 'timeline:new', ->
      $scope.addIndex = 2
      $scope.toggleDropdown()

    $scope.$on 'filters:show', ->
      $scope.filters = true
      $scope.sidebar = true

    $scope.$on 'reset', ->
      $scope.filters = false
      $scope.addIndex = null

    $scope.$on '$routeChangeSuccess', (evt, data) ->
      $scope.isRoadmap = data.loadedTemplateUrl == 'roadmap.html'
]
