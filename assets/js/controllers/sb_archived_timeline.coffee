angular.module('roadmap').controller 'sbArchivedTimelineCtrl', [
  '$scope', '$rootScope', 'timeline',
  ($scope, $rootScope, timeline) ->
    $scope.activeTimeline = timeline.get()

    $scope.noMilestones = -> Object.keys($scope.activeTimeline.milestones).length == 0
]
