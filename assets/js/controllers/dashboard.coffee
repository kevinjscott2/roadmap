angular.module('roadmap').controller 'dashboardCtrl', [
  '$scope', '$rootScope', '$timeout', 'roadmap',
  ($scope, $rootScope, $timeout, roadmap) ->
    pages = ['roadmaps_list', 'add_new_roadmap']
    page = 0

    $scope.importedRoadmap = null
    $scope.onlyOwn = true
    $scope.usersSidebar = false

    $scope.loadRoadmap = (roadmap_id) ->
      roadmap.go roadmap_id

    $scope.loadRoadmaps = ->
      roadmap.all (roadmaps) ->
        $scope.roadmaps = roadmaps

    $scope.page = -> pages[page] + '.html'

    $scope.go = (index) ->
      return unless index

      page = index

      if index > 0
        $timeout ->
          document.getElementById('add-new-roadmap').focus()
        , 100

      $rootScope.$broadcast 'reset'

    $scope.addRoadmap = (rm) ->
      return unless rm

      rm = rm.trim()

      roadmap.add rm, (roadmap_id) ->
        $rootScope.$broadcast 'notify', {
          text: 'Roadmap added'
          type: 'info'
        }
        $rootScope.$broadcast 'menu:close'
        roadmap.go roadmap_id

    $scope.onFileSelect = ($files) ->
      roadmap.import $files[0], (importedRoadmap) ->
        roadmap.go importedRoadmap.id
        $rootScope.$broadcast 'menu:close'

    $scope.isOwner = (roadmap) -> roadmap.owner_id == $scope.user().id
    $scope.filteredRoadmaps = ->
      if $scope.onlyOwn
        _.filter $scope.roadmaps, $scope.isOwner
      else
        $scope.roadmaps

    $scope.toggleUsersSidebar = -> $scope.usersSidebar = !$scope.usersSidebar

    $scope.$on 'sign:in', -> $scope.loadRoadmaps()

    $scope.loadRoadmaps()
]
