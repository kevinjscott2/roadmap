window.GOOGLE_SCOPE = 'https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email'

switch window.location.host
  when 'road.map:3000', 'roadmap-staging.mlsdev.com'
    window.API_HOST = 'http://roadmapbackend-staging.mlsdev.com/api/v1/'
    window.WEBSOCKET_HOST = 'http://roadmap-staging.mlsdev.com:80/'
    window.BACKEND_HOST = 'http://roadmapbackend-staging.mlsdev.com/'

    window.GOOGLE_CLIENT_ID = '924604890950-r4cghie58eh6ua6kqjhg6usftcnsqoug.apps.googleusercontent.com'
    window.GOOGLE_API_KEY = 'AIzaSyCrKcZ8WLWeHLPvCDWWdXKWE3zEt45LLVs'

    window.GITHUB_URL = 'https://github.com'
    window.GITHUB_CLIENT_ID = '88ef9b56bfaaafa23aef'

    window.FACEBOOK_APP_ID = '490715801016623'

  when 'roadmap.mlsdev.com'
    window.API_HOST = 'http://roadmapbackend.mlsdev.com/api/v1/'
    window.WEBSOCKET_HOST = 'http://roadmap.mlsdev.com:80/'
    window.BACKEND_HOST = 'http://roadmapbackend.mlsdev.com/'

    window.GOOGLE_CLIENT_ID = '924604890950-r4cghie58eh6ua6kqjhg6usftcnsqoug.apps.googleusercontent.com'
    window.GOOGLE_API_KEY = 'AIzaSyCrKcZ8WLWeHLPvCDWWdXKWE3zEt45LLVs'

    window.GITHUB_URL = 'https://github.com'
    window.GITHUB_CLIENT_ID = '88ef9b56bfaaafa23aef'

    window.FACEBOOK_APP_ID = '490715801016623'

  when 'roadmap.espninnovation.com'
#    change after backend deploy
    window.API_HOST = 'http://backend.roadmap.espninnovation.com/api/v1/'
    window.WEBSOCKET_HOST = 'http://roadmap.espninnovation.com:80/'
    window.BACKEND_HOST = 'http://backend.roadmap.espninnovation.com/'

    window.GOOGLE_CLIENT_ID = '566630928143-nhn4mcbqvde325ld8s8i3mraf1s1lhtq.apps.googleusercontent.com'
    window.GOOGLE_API_KEY = 'AIzaSyCrKcZ8WLWeHLPvCDWWdXKWE3zEt45LLVs'

    window.GITHUB_URL = 'https://code.espn.com'
    window.GITHUB_CLIENT_ID = '53f8f2493f5634ff0fdc'

    window.FACEBOOK_APP_ID = '258707560994182'

#http://roadmap.espninnovation.com/
#http://staging.roadmap.espninnovation.com/
#http://dev.roadmap.espninnovation.com/
