angular.module('roadmap').service 'user', ($rootScope, storage, users) ->
  user_data = {
    email: ''
    second_email: ''
    name: ''
    token: ''
    provider: ''
  }

  PROVIDERS = {
    GOOGLE: 0
    FACEBOOK: 1
    GITHUB: 2
  }

  signinSuccess = ->
    storage.set 'user', user_data

    $rootScope.$broadcast 'notify', {
      type: 'success'
      text: 'Successfully signed in'
    }
    $rootScope.$broadcast 'sign:in'

  signinFailure = ->
    $rootScope.$broadcast 'notify', {
      type: 'failure'
      text: 'Unable to authorize'
    }

  signinBackend = (provider, access_token) ->
    users.auth {
      provider: provider
      access_token: access_token
    },
      (success) ->
        user_data.id = success.id
        user_data.email = success.email
        user_data.second_email = success.second_email
        user_data.token = success.token
        user_data.role = success.role
        user_data.image = success.avatar

        user_data.name = user_data.email if !user_data.name or user_data.name.length == 0

        signinSuccess()
      ->
        signinFailure()

  signinGoogle = ->
    console.log '[User service] Signin with Google'

    user_data.provider = 'google'

    called = false

    gapi.auth.signIn {
      clientid: GOOGLE_CLIENT_ID
      cookiepolicy: 'single_host_origin'
      scope: GOOGLE_SCOPE
      callback: (result) ->
        if called
          if result.status.signed_in
            gapi.client.setApiKey GOOGLE_API_KEY

            gapi.client.load 'plus', 'v1', ->
              req = gapi.client.plus.people.get {userId: 'me'}
              req.execute (resp) ->
                user_data.name = resp.displayName if resp.displayName and resp.displayName.length > 0

                storage.set 'user', user_data

            signinBackend 'google', result.access_token
        called = true
    }

  signinFacebook = ->
    console.log '[User service] Signin with Facebook'

    user_data.provider = 'facebook'

    FB.login (response) ->
      if response.authResponse
        FB.api '/me', (meResponse) ->
          $rootScope.$apply ->
            user_data.name = meResponse.name
            user_data.email = meResponse.email

        signinBackend 'facebook', response.authResponse.accessToken
      else
        signinFailure()
    , {scope: 'email'}

  signinGithub = (token) ->
    console.log '[User service] Signin with Github'

    if token
      signinBackend 'github', token
    else
      client_id = GITHUB_CLIENT_ID
      url = GITHUB_URL + '/login/oauth/authorize?client_id=' + client_id + '&scope=user:email'

      location.assign url

  init = ->
    # load user data from cookies
    user_data = storage.get 'user' if storage.get 'user'

    # load facebook SDK
    window.fbAsyncInit = ->
      FB.init {
        appId: FACEBOOK_APP_ID
        xfbml: true
        version: 'v2.0'
      }

    ((d, s, id) ->
      js
      fjs = d.getElementsByTagName(s)[0]
      return if d.getElementById(id)
      js = d.createElement(s)
      js.id = id
      js.src = '//connect.facebook.net/en_US/sdk.js'
      fjs.parentNode.insertBefore js, fjs)(document, 'script', 'facebook-jssdk')

  @signin = (provider) ->
    switch provider
      when PROVIDERS.GOOGLE
        signinGoogle()
      when PROVIDERS.FACEBOOK
        signinFacebook()
      when PROVIDERS.GITHUB
        signinGithub()
      else
        signinFailure()

  @updateEmail = (callback) ->
    users.update {
      id: user_data.id
      email: user_data.email
      second_email: user_data.second_email
      role_id: user_data.role
    }, ->
      storage.set 'user', user_data
      callback()

  @signinGithubToken = (token) -> signinGithub token
  @signedin = -> user_data and user_data.token.length > 0
  @getUser = -> user_data
  @getToken = -> user_data.token
  @logout = -> user_data.token = ''
  @providers = -> PROVIDERS

  @setImage = (image) ->
    user_data.image = image

    storage.set 'user', user_data

  @isAdmin = -> user_data.role == 1

  @reset = ->
    user_data = {
      email: ''
      second_email: ''
      name: ''
      token: ''
      provider: ''
    }
    storage.set 'user', user_data

  init()

  undefined
