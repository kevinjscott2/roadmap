angular.module('roadmap').service 'timeline', ($rootScope, $filter, user, timelines, milestones, label, filter) ->
  timelines_data = {}
  dates = {
    true: {}
    false: {}
  }
  roadmap = null
  selectedTimeline = null
  selectedMilestone = null

  loaded = false

  pushMilestone = (milestone) ->
    fromUtcDate = new Date(milestone.date_end)
    index = milestone.description.indexOf '\n'

    milestone.date = fromUtcDate.getTime()
    milestone.isMultiline = (index > -1 and index < milestone.description.length - 1)

    timelines_data[milestone.timeline_id].milestones[milestone.id] = milestone

    if timelines_data[milestone.timeline_id].visible and label.visible(milestone.label_id)
      dates[timelines_data[milestone.timeline_id].is_archive][milestone.date] = 0


  loadMilestones = ->
    push = (milestones) ->
      angular.forEach milestones, (milestone) ->
        pushMilestone milestone if timelines_data[milestone.timeline_id]

      $rootScope.$broadcast 'milestones:loaded'
      $rootScope.$broadcast 'updated'

    milestones.query {
      roadmap_id: roadmap.id
    }, (success) ->
      push success

  updateDates = ->
    dates = {
      true: {}
      false: {}
    }

    angular.forEach timelines_data, (timeline) ->
      angular.forEach timeline.milestones, (milestone) ->
        pushMilestone milestone if timelines_data[milestone.timeline_id]

  loadTimelines = (callback) ->
    processing = (success) ->
      loaded = true

      angular.forEach success, (timeline) ->
        timelines_data[timeline.id] = timeline
        timelines_data[timeline.id].visible = true
        timelines_data[timeline.id].milestones = {}

      if filter.timelines().length > 0
        angular.forEach timelines_data, (timeline) -> timeline.visible = false
        angular.forEach filter.timelines(), (id) -> timelines_data[id].visible = true

      loadMilestones()
      callback() if callback

    if user.signedin()
      timelines.query {
        roadmap_id: roadmap.id
      }, (success) ->
        processing success
    else
      timelines.query {
        roadmap_id: roadmap.id
      }, (success) ->
        processing success

  @setRoadmap = (r, callback) ->
    if roadmap != r
      roadmap = r

      timelines_data = {}
      loaded = false

      callback()

  @getRoadmap = -> roadmap

  @active = (callback) ->
    if loaded
      result = {}
      angular.forEach timelines_data, (timeline, id) ->
        result[id] = timeline unless timeline.is_archive
      callback result
    else
      loadTimelines ->
        result = {}
        angular.forEach timelines_data, (timeline, id) ->
          result[id] = timeline unless timeline.is_archive
        callback result

  @archived = (callback) ->
    if loaded
      result = {}
      angular.forEach timelines_data, (timeline, id) ->
        result[id] = timeline if timeline.is_archive
      callback result
    else
      loadTimelines ->
        result = {}
        angular.forEach timelines_data, (timeline, id) ->
          result[id] = timeline if timeline.is_archive
        callback result

  @forceLoad = (callback) ->
    timelines_data = {}

    loadTimelines ->
      result = {}
      angular.forEach timelines_data, (timeline, id) ->
        result[id] = timeline unless timeline.is_archive
      callback result

  @dates = (archive, callback) ->
    updateDates()
    callback dates[archive]

  @addTimeline = (title, callback) ->
    timelines.save {
      roadmap_id: roadmap.id
      title: title
    }, (success) ->
      console.log 'adding'
      timelines_data[success.id] = {
        id: success.id
        title: title
        is_archive: false
        visible: true
        milestones: {}
      }
      callback()

  @archive = (id, callback) ->
    if timelines_data[id]
      timelines.update {
        roadmap_id: roadmap.id
        id: id
        title: timelines_data[id].title
        is_archive: true
      }, ->
        timelines_data[id].is_archive = true
        $rootScope.$broadcast 'archived'
        callback()

  @restore = (id, callback) ->
    if timelines_data[id]
      timelines.update {
        roadmap_id: roadmap.id
        id: id
        title: timelines_data[id].title
        is_archive: false
      }, ->
        timelines_data[id].is_archive = false
        loaded.active = false
        callback()

  @remove = (id, callback) ->
    if timelines_data[id]
      timelines.delete {
        id: id,
        h: ''
      }, ->
        delete timelines_data[id]
        callback()

  @addMilestone = (milestone, callback) ->
    pushMilestone milestone if timelines_data[milestone.timeline_id]
    callback()

  @removeMilestone = (id, callback) ->
    m = {
      milestone: null
      index: -1
    }
    angular.forEach data.milestones.active.raw, (milestone, index) ->
      if milestone.id == id
        m = {
          milestone: milestone
          index: index
        }

    if m.milestone
      milestones.delete {
        id: m.milestone.id
      }, ->
        data.milestones.active.raw.splice m.index, 1
        combineMilestones()

        callback()

  @select = (timeline) -> selectedTimeline = timeline
  @get = -> selectedTimeline

  @newMilestone = (timeline_id) ->
    selectedMilestone = {
      timeline_id: timeline_id
      description: ''
      label_id: null
      sub_items: []
    }

  @setMilestone = (milestone) -> selectedMilestone = milestone
  @getActiveMilestone = -> selectedMilestone
  @clearMilestone = -> selectedMilestone = null

  @copyTo = (timelineId, roadmapId, success, failure) ->
    timelines.copy {
      id: timelineId
      roadmap_id: roadmapId
    },
      -> success()
      -> failure()

  @moveTo = (timelineId, roadmapId, success, failure) ->
    timelines.move {
      id: timelineId
      roadmap_id: roadmapId
    },
      ->
        delete timelines_data[timelineId]
        $rootScope.$broadcast 'updated'

        success()
      -> failure()


  # sockets

  @addFromSocket = (timeline) ->
    timelines_data[timeline.id] = timeline
    timelines_data[timeline.id].visible = true
    timelines_data[timeline.id].milestones = {}

    $rootScope.$broadcast 'updated'

  @archiveFromSocket = (timeline) ->
    $rootScope.$apply ->
      timelines_data[timeline.id].is_archive = true

      $rootScope.$broadcast 'archived'

  @restoreFromSocket = (timeline) ->
    $rootScope.$apply ->
      timelines_data[timeline.id].is_archive = false

      $rootScope.$broadcast 'updated'

  @updateTitleFromSocket = (timeline) ->
    $rootScope.$apply ->
      timelines_data[timeline.id].title = timeline.title

  @copyMoveFromSocket = (timeline, deltas) ->
    if deltas.roadmap[0].id != roadmap.id and deltas.roadmap[1].id == roadmap.id
      loadTimelines()

    if deltas.roadmap[0].id == roadmap.id and deltas.roadmap[1].id != roadmap.id
      $rootScope.$apply ->
        delete timelines_data[timeline.id]

        $rootScope.$broadcast 'updated'

    if deltas.self and deltas.roadmap[0].id == deltas.roadmap[1].id
      $rootScope.$apply ->
        timelines_data[deltas.self[1].id] = deltas.self[1]
        timelines_data[timeline.id].visible = true
        timelines_data[timeline.id].milestones = timelines_data[deltas.self[0].id].milestones

        $rootScope.$broadcast 'updated'

  @addMilestoneFromSocket = (milestone, deltas) ->
    if timelines_data[milestone.timeline.id]
      $rootScope.$apply ->
        pushMilestone {
          id: milestone.id
          description: deltas.description[1]
          label_id: deltas.label_id[1]
          date_end: milestone.date_end
          timeline_id: milestone.timeline.id
          sub_items: []
        }

        $rootScope.$broadcast 'updated'

  @editMilestoneFromSocket = (milestone, deltas) ->
    if timelines_data[milestone.timeline.id] and timelines_data[milestone.timeline.id].milestones[milestone.id]
      $rootScope.$apply ->
        m = timelines_data[milestone.timeline.id].milestones[milestone.id]

        m.description = deltas.description[1] if deltas.description
        m.label_id = deltas.label[1].id if deltas.label

        if deltas.date_end
          m.date_end = deltas.date_end[1]
          m.date = new Date(m.date_end).valueOf()

          $rootScope.$broadcast 'updated'

  @addSubitemFromSocket = (subitem, deltas) ->
    if timelines_data[subitem.milestone.timeline.id] and timelines_data[subitem.milestone.timeline.id].milestones[subitem.milestone.id]
      $rootScope.$apply ->
        timelines_data[subitem.milestone.timeline.id].milestones[subitem.milestone.id].sub_items.push {
          id: deltas.id[1]
          name: deltas.name[1]
        }

        $rootScope.$broadcast 'updated'

  @removeSubitemFromSocket = (subitem) ->
    if timelines_data[subitem.milestone.timeline.id] and timelines_data[subitem.milestone.timeline.id].milestones[subitem.milestone.id]
      $rootScope.$apply ->
        arr = timelines_data[subitem.milestone.timeline.id].milestones[subitem.milestone.id].sub_items
        timelines_data[subitem.milestone.timeline.id].milestones[subitem.milestone.id].sub_items = _.without arr, _.findWhere arr, {id: subitem.id}

  @renameSubitemFromSocket = (subitem) ->
    if timelines_data[subitem.milestone.timeline.id] and timelines_data[subitem.milestone.timeline.id].milestones[subitem.milestone.id]
      $rootScope.$apply ->
        arr = timelines_data[subitem.milestone.timeline.id].milestones[subitem.milestone.id].sub_items
        _.findWhere(arr, {id: subitem.id}).name = subitem.name

  @reorderSubitemsFromSocket = (milestoneId, timelineId, subitemsOrder) ->
    if timelines_data[timelineId] and timelines_data[timelineId].milestones[milestoneId]
      oldOrder = timelines_data[timelineId].milestones[milestoneId].sub_items
      newOrder = []

      _.each subitemsOrder, (order) ->
        newOrder.push _.findWhere(oldOrder, {id: order})

      $rootScope.$apply ->
        timelines_data[timelineId].milestones[milestoneId].sub_items = newOrder

  undefined
