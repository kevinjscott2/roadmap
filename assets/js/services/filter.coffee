angular.module('roadmap').service 'filter', ->
  timelines = []
  labels = []

  @set = (hash) ->
    hashids = new Hashids 'roadmap-frontend'
    hashes = hash.split ','

    timelines = hashids.decrypt hashes[0]
    labels = hashids.decrypt hashes[1]

  @timelines = ->
    timelines

  @labels = ->
    labels

  undefined
