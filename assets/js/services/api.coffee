angular.module('roadmap').config ($httpProvider) ->
  $httpProvider.defaults.useXDomain = true
  $httpProvider.defaults.headers.common['Access-Control-Allow-Origin']

  $httpProvider.interceptors.push ($q, $rootScope, storage) ->
    {
      request: (config) ->
        console.log '[Resource] onRequest'
        $rootScope.$broadcast 'request', true

        if storage.get('user') and storage.get('user').token
          if config.params
            config.params.token = storage.get('user').token
          else
            config.data = {} unless config.data
            config.data.token = storage.get('user').token

        config

      response: (response) ->
        console.log '[Resource] onResponse'
        $rootScope.$broadcast 'request', false

        response

      responseError: (response) ->
        console.log '[Resource] onResponseError'
        $rootScope.$broadcast 'request', false

        if response.status == 401
          console.log '[Resource] unauthorized'

          $rootScope.$broadcast 'notify', {
            type: 'failure'
            text: 'Unauthorized'
          }

          $rootScope.$broadcast 'edit:mode', false
          $rootScope.$broadcast 'logout'

        $q.reject response
    }

angular.module('roadmap').factory 'users', ['$resource', ($resource) ->
   return $resource API_HOST + 'users/:type/:id', {id: '@id'}, {
     auth: {method: 'POST', params: {type: 'auth'}}
     search: {method: 'GET', params: {type: 'search'}, isArray: true}
     update: {method: 'PUT'}
     delete: {method: 'DELETE', headers: {'Content-Type': 'application/json'}}
   }
]

angular.module('roadmap').factory 'timelines', ['$resource', ($resource) ->
  return $resource API_HOST + 'timelines/:id/:action', {id: '@id'}, {
    update: {method: 'PUT'}
    copy: {method: 'POST', params: {action: 'copy'}}
    move: {method: 'PUT', params: {action: 'move'}}
  }
]

angular.module('roadmap').factory 'milestones', ['$resource', ($resource) ->
  return $resource API_HOST + 'milestones/:id', {id: '@id'}, {
    update: {method: 'PUT'}
  }
]

angular.module('roadmap').factory 'subitems', ['$resource', ($resource) ->
  return $resource API_HOST + 'sub_items/:type/:id', {id: '@id'}, {
    update: {method: 'PUT'}
    sort: {method: 'PUT', params: {type: 'sort'}}
  }
]

angular.module('roadmap').factory 'labels', ['$resource', ($resource) ->
  return $resource API_HOST + 'labels/:type/:id', {id: '@id'}, {
    update: {method: 'PUT'}
    colors: {method: 'GET', params: {type: 'colors'}, isArray: true}
  }
]

angular.module('roadmap').factory 'roadmaps', ['$resource', ($resource) ->
  return $resource API_HOST + 'roadmaps/:type/:id', {id: '@id'}, {
    active: {method: 'GET', params: {type: 'active'}, isArray: true}
    archive: {method: 'GET', params: {type: 'archive'}, isArray: true}
    import: {method: 'POST', params: {type: 'import'}, headers: {'Content-Type': 'multipart/form-data'}}
    export: {method: 'GET', params: {type: 'export'}}
    update: {method: 'PUT'}
    byHash: {method: 'GET', isArray: false}
    by_milestone: {method: 'GET', params: {type: 'by_milestone'}}
  }
]

angular.module('roadmap').factory 'members', ['$resource', ($resource) ->
  return $resource API_HOST + 'user_roadmaps/:id', {id: '@id'}, {
    update: {method: 'PUT'}
    delete: {method: 'DELETE', headers: {'Content-Type': 'application/json'}}
  }
]

angular.module('roadmap').factory 'activities', ['$resource', ($resource) ->
  return $resource API_HOST + 'activities', {}, {}
]

angular.module('roadmap').factory 'subscriptions', ['$resource', ($resource) ->
  return $resource API_HOST + 'subscriptions', {}, {}
]
