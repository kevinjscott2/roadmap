angular.module('roadmap').service 'subscription', (subscriptions) ->
  subscriptionsData = []

  @all = (callback) ->
    subscriptions.query {t: 0}, (success) ->
      subscriptionsData = success
      callback subscriptionsData

  @subscribe = (roadmapId, callback) ->
    subscriptions.save {roadmap_id: roadmapId}, ->
      callback()

  @unsubscribe = (roadmapId, callback) ->
    subscriptions.remove {roadmap_id: roadmapId}, ->
      callback()

  undefined
