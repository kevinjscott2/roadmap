angular.module('roadmap').service 'roadmap_users', (user, users) ->
  user_data = []
  selected_user = null

  @all = (callback) ->
    if user_data.length == 0
      users.query {
        page: 1
        per_page: 100
      }, (success) ->
        user_data = success
        callback user_data
    else
      callback user_data

  @add = (user, success_callback, failure_callback) ->
    users.save {
      email: user.email
      role: user.role
    },
      (success) ->
        u = {
          id: success.id
          email: user.email
          role: user.role
        }

        user_data.push u

        success_callback u
      ->
        failure_callback()

  @setRole = (u, role_id, callback) ->
    users.update {
      id: u.id
      email: u.email
      role_id: role_id
    }, ->
      u.role = role_id
      callback()

  @show = -> selected_user

  @select = (user) -> selected_user = user

  @remove = (user, successCallback, failureCallback) ->
    users.delete {id: user.id},
      ->
        user_data = _.reject user_data, (u) -> user.id = u.id
        successCallback()
      failureCallback

  undefined
