angular.module('roadmap').service 'storage', ($cookies) ->
  @set = (key, value) ->
    $cookies[key] = JSON.stringify value

  @get = (key) ->
    JSON.parse($cookies[key]) if $cookies[key]

  undefined
