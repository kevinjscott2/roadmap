angular.module('roadmap').service 'label', ($rootScope, $location, user, labels, filter) ->
  labels_data = []
  colors = []

  @load = (roadmap_id) ->
    labels.colors {}, (success) ->
#      shit...
      angular.forEach success, (color) ->
        real_color = ''
        angular.forEach color, (n) ->
          real_color += n

        colors.push real_color

      processing = (labels_success) ->
        labels_data = labels_success

        angular.forEach labels_data, (label) ->
          label.visible = false

        angular.forEach labels_data, (label) ->
          if filter.labels().indexOf(label.id) > -1
            label.visible = true
          else
            label.visible = false

        $rootScope.$broadcast 'updated'

      if user.signedin()
        labels.query {
          roadmap_id: roadmap_id
        }, (labels_success) ->
          processing labels_success
      else
        labels.query {
          roadmap_id: roadmap_id
        }, (labels_success) ->
          processing labels_success

  @all = ->
    labels_data

  @visible = (id) ->
    result = true
    filtered = false

    angular.forEach labels_data, (label) ->
      result = label.visible if label.id == id
      filtered = true if label.visible

    if filtered
      result
    else
      true

  @getColor = (id) ->
    label = _.findWhere labels_data, {id: id}
    if label then colors[label.color_id] else '#9b59b6'

  @getName = (id) -> _.findWhere(labels_data, {id: id}).text

  @filter = (visible) ->
    angular.forEach labels_data, (label) ->
      if visible.indexOf(label.id) > -1
        label.visible = true
      else
        label.visible = false

  @changeNameFromSocket = (updatedLabel) ->
    label = _.findWhere labels_data, {id: updatedLabel.id}
    $rootScope.$apply -> label.text = updatedLabel.text

  undefined
