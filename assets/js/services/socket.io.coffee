angular.module('roadmap').service 'socketIO', [
  'activity', 'timeline', 'user', 'roadmap', 'label',
  (activity, timeline, user, roadmap, label) ->
    unless socket
      socket = io.connect WEBSOCKET_HOST

      socket.on 'connect', -> console.log 'socket: connected'
      socket.on 'disconnect', -> console.log 'socket: disconnected'

      socket.on 'message', (data) ->
        parsedData = JSON.parse data

        activity.add parsedData

        return if parsedData.user.id == user.getUser().id

        switch parsedData.entity_type
          when 'Timeline'
            if parsedData.deltas.roadmap
              if parsedData.deltas.roadmap[0] == null
                timeline.addFromSocket parsedData.entity_details

              if parsedData.deltas.roadmap[0] and parsedData.deltas.roadmap[1] or parsedData.deltas.self
                timeline.copyMoveFromSocket parsedData.entity_details, parsedData.deltas

            if parsedData.deltas.is_archive
              if !parsedData.deltas.is_archive[0] and parsedData.deltas.is_archive[1]
                timeline.archiveFromSocket parsedData.entity_details

              if parsedData.deltas.is_archive[0] and !parsedData.deltas.is_archive[1]
                timeline.restoreFromSocket parsedData.entity_details

            if parsedData.deltas.title
              timeline.updateTitleFromSocket parsedData.entity_details

          when 'Milestone'
            if parsedData.deltas.resorted
              timeline.reorderSubitemsFromSocket parsedData.entity_details.id, parsedData.deltas.timeline.id, parsedData.deltas.sub_items
            else
              if parsedData.deltas.description
                if parsedData.deltas.description[0] == null
                  timeline.addMilestoneFromSocket parsedData.entity_details, parsedData.deltas

                if parsedData.deltas.description[0] and parsedData.deltas.description[1]
                  timeline.editMilestoneFromSocket parsedData.entity_details, parsedData.deltas

              if parsedData.deltas.label
                timeline.editMilestoneFromSocket parsedData.entity_details, parsedData.deltas

              if parsedData.deltas.date_end
                timeline.editMilestoneFromSocket parsedData.entity_details, parsedData.deltas

          when 'SubItem'
            if parsedData.deltas.name
              if parsedData.deltas.name[0] == null
                timeline.addSubitemFromSocket parsedData.entity_details, parsedData.deltas

              if parsedData.deltas.name[1] == null
                timeline.removeSubitemFromSocket parsedData.entity_details

              if parsedData.deltas.name[0] and parsedData.deltas.name[1]
                timeline.renameSubitemFromSocket parsedData.entity_details

          when 'UserRoadmap'
            if parsedData.deltas.rights
              parsedData.deltas.rights[0] == null

          when 'Roadmap'
            if parsedData.deltas.name
              roadmap.changeNameFromSocket parsedData.deltas.name[1]

            if parsedData.deltas.is_private
              roadmap.changePrivacyFromSocket parsedData.deltas.is_private[1]

          when 'Label'
            label.changeNameFromSocket parsedData.entity_details

    socket
]
