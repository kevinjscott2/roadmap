initActivityEntity = (activity) ->
  new this[activity.entity_type + 'Entity'](activity)

class BaseActivityEntity

  constructor: (activity)->
    @activity = activity

#    Ugly workaround for converting utc unix timestamp to date in current timezone. Use moment.js instead
#    createdAt = new Date(@activity.created_at * 1000).toString().split(' ')
#    createdAt.pop()
#    createdAt[-1] = 'UTC'
#    @activity.created_at = new Date(createdAt.join(' '))

  deltas: ->
    @activity.deltas

  action: ->
    if @deltas()?.id
      if @deltas().id[0] == null
        actionName = 'created'
      else if @deltas().id[1] == null
        actionName = 'deleted'
    else
      actionName = 'updated'
    actionName

  actionDetails: ->
    [@action(), @entityName(), @title()]

  user: ->
    @activity.user.email

  destination: ->
    ''

  title: ->
    ''

  entityName: ->
    ''

  createdAt: ->
    @activity.created_at

  format: (parts)->
    parts = [parts] unless _.isArray(parts)
    _.map(_.compact(parts), (item)->
      if _.isArray(item)
        @format(item)
      else if _.isDate(item)
        $.datepicker.formatDate('dd M, D', item)
      else if _.isObject(item)
        '<a href="' + item.link + '">' + item.title + '</a>'
      else
        item
    ).join(' ')

  quote: (text)->
    "'" + text + "'"

class ActivityEntity extends BaseActivityEntity

  actionDetails: ->
    description = super()
    if @action() == 'updated'
      actionName = @actionDetailsForUpdate(description)
      description[0] = actionName if actionName
    description

  actionDetailsForUpdate: (description)->
    ''

class RoadmapEntity extends ActivityEntity

  actionDetailsForUpdate: (description)->
    if @deltas()?.name
      actionName = 'renamed'
      description.push('from', @deltas().name[0])
    else if @deltas()?.is_private
      actionName = 'marked'
      description.push('as', if @deltas().is_private[1] then 'private' else 'public')
    else if @deltas()?.is_archive
      actionName = 'moved'
      description.push((if @deltas().is_archive[1] then 'to' else 'out from'), 'archive')
    actionName

  entityName: ->
    'current roadmap'

class TimelineEntity extends ActivityEntity

  action: ->
    actionName = super()
    if actionName == 'updated'
      if @deltas()?.roadmap and not @deltas()?.self
        actionName = 'moved'
      else if @deltas()?.roadmap and @deltas()?.self
        actionName = 'copied'
    actionName

  actionDetailsForUpdate: (description)->
    if @deltas()?.title
      actionName = 'renamed'
      description.push('from', @quote(@deltas().title[0]))
    else if @deltas()?.is_archive
      actionName = 'moved'
      description.push((if @deltas().is_archive[1] then 'to' else 'out from'), 'archive')
    actionName

  title: ->
    @quote(@activity.entity_details.title)

  entityName: ->
    'timeline'

  destination: ->
    dest = ''
    actionName = @action()
    if _.contains(['moved', 'copied'], actionName)
      roadmapFrom = @deltas()?.roadmap[0]
      roadmapTo = @deltas()?.roadmap[1]
      dest = ['from', {title: roadmapFrom?.name, link: '/r/' + roadmapFrom?.hashed_id}, 'roadmap']
      if actionName == 'moved'
        dest.push(
          'to',
          {title: roadmapTo.name, link: '/r/' + roadmapTo.hashed_id},
          'roadmap'
        )
    dest

class MilestoneEntity extends ActivityEntity

  action: ->
    if @deltas()?.resorted
      actionName = 'reordered subitems of'
    else
      if @deltas()?.id
        if @deltas().id[0] == null
          actionName = 'created'
        else if @deltas().id[1] == null
          actionName = 'deleted'
      else
        actionName = 'updated'
    actionName

  actionDetailsForUpdate: (description) ->
    if @deltas()?.resorted
      actionName = 'resorted'
      description.push('from', new Date(@deltas().date_end[0] + ' UTC'))
    else if @deltas()?.description
      oldDesc = @deltas().description[0].substring(0, Math.min(100, @deltas().description[0].length))
      oldDesc += '...' if @deltas().description[0].length > 100

      actionName = 'changed description of'
      description.push('from', @quote(oldDesc))
    else if @deltas()?.date_end
      actionName = 'changed date end of'
      description.push('from', new Date(@deltas().date_end[0] + ' UTC'))

    actionName

  permalink: ->
    title: $.datepicker.formatDate('dd M, D', new Date(@activity.entity_details.date_end + ' UTC'))
    link: '/m/' + @activity.entity_details.hashed_id

  entityName: ->
    'milestone'

  title: ->
    @permalink()

  destination: ->
    ['at timeline', @activity.entity_details.timeline.title]

class SubItemEntity extends ActivityEntity

  actionDetailsForUpdate: (description)->
    if @deltas()?.name
      actionName = 'renamed'
      description.push('from', @quote(@deltas().name[0]))
    actionName

  entityName: ->
    'sub item'

  title: ->
    @quote(@activity.entity_details.name)

  destination: ->
    [
      'at milestone',
      {
        title: $.datepicker.formatDate('dd M, D', new Date(@activity.entity_details.milestone.date_end + ' UTC')),
        link: '/m/' + @activity.entity_details.milestone.hashed_id
      },
      'of timeline',
      @activity.entity_details.milestone.timeline.title
    ]

class UserRoadmapEntity extends ActivityEntity

  actionDetails: ->
    actionName = @action
    if actionName == 'created'
      description = ['added', @title(), 'to']
    else if actionName == 'deleted'
      description = ['deleted', @title(), 'from']
    else
      role = if @activity.entity_details.rights == 'reader' then 'a normal user' else 'an admin'
      description = ['made', @title(), role, 'on']
    description

  title: ->
    @activity.entity_details.user.email

  destination: ->
    'this board'


angular.module('roadmap').service 'activity', [
  '$rootScope', 'activities', 'members',
  ($rootScope, activities, members) ->
    activities_data = []

    @all = (roadmapId, callback) ->
      if activities_data.length > 0
        callback activities_data
      else
        members.query {roadmap_id: roadmapId}, ->
          activities.query {
            roadmap_id: roadmapId
            page: 1
            per_page: 10
          }, (success) ->
            angular.forEach success, (activity) ->
              if this[activity.entity_type + 'Entity']
                activities_data.push initActivityEntity(activity)

            callback activities_data

    @add = (activity) ->
      $rootScope.$apply ->
        activities_data.unshift initActivityEntity(activity)
        $rootScope.$broadcast 'update:activity'

    @clear = -> activities_data = []

    undefined
]
