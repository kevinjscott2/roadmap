angular.module('roadmap').service 'roadmap', ($rootScope, $location, $timeout, user, roadmaps, members, subscriptions, $upload, activity) ->
  roadmap_data = []
  currentRoadmap = null
  page = 1
  perPage = 50

  findById = (roadmap_id) ->
    object = null

    angular.forEach roadmap_data, (roadmap, index) ->
      if roadmap.id == roadmap_id
        currentRoadmap = roadmap
        object = {
          roadmap: roadmap
          index: index
        }

    object

  urlName = (name) -> name.toLowerCase().replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '').split(' ').join('-')

  loadRoadmaps = (callback) ->
    if user.signedin()
      roadmaps.active {
        page: page
        per_page: perPage
      }, (success) ->
        angular.forEach success, (roadmap) ->
          roadmap.is_archive = false if typeof roadmap.is_archive == 'undefined'
          roadmap.is_private = true if typeof roadmap.is_private == 'undefined'
        roadmap_data = roadmap_data.concat success

        page++

        callback() if callback

  fallback = (hash, callback, failureCallback) ->
    console.log '[Roadmap service] Fallback roadmap load'

    roadmaps.byHash {
      hash: hash
    },
      (success) ->
        roadmap_data.push success
        callback success
      -> failureCallback() if failureCallback

  magickURL = (url) ->
    $location.path url
    $location.replace()
    history.pushState null, '', url

  @all = (callback) ->
    loadRoadmaps -> callback roadmap_data

  @findById = (roadmap_id, callback) ->
    if roadmap_data.length == 0
      loadRoadmaps ->
        callback findById(roadmap_id)
    else
      callback findById(roadmap_id)

  @findByHash = (roadmap_hash, callback, failureCallback) ->
    if roadmap_data.length == 0
      if user.signedin()
        loadRoadmaps ->
          object = null

          object = _.findWhere roadmap_data, {hash: roadmap_hash}

          if object
            currentRoadmap = object
            callback object
          else
            fallback roadmap_hash,
              (success) ->
                currentRoadmap = success
                callback success
              failureCallback
      else
        fallback roadmap_hash,
          (success) -> callback success
          failureCallback
    else
      callback _.findWhere roadmap_data, {hash: roadmap_hash}

  @findByMilestone = (milestone_hash, callback) ->
    roadmaps.by_milestone {
      milestone_hash: milestone_hash
    }, (success) ->
      callback success

  @add = (roadmap_name, callback) ->
    roadmaps.save {
      name: roadmap_name
    }, (success) ->
      roadmap = {
        id: success.id
        hash: success.hash
        name: roadmap_name
      }

      roadmap_data.push roadmap

      callback roadmap.id

  @update = (roadmap, callback) ->
    roadmaps.update {
      id: roadmap.id
      name: roadmap.name
      is_archive: roadmap.is_archive
      is_private: roadmap.is_private
    }, ->
      callback()

  @close = (roadmap_id, callback) ->
    roadmap = findById roadmap_id

    if roadmap
      roadmaps.update {
        id: roadmap.roadmap.id
        name: roadmap.roadmap.name
        is_archive: true
        is_private: roadmap.roadmap.is_private
      }, ->
        roadmap_data.splice roadmap.index, 1
        $location.path '/'

        callback()

  @leave = (roadmap_id, success_callback, failure_callback) ->
    members.query {
      roadmap_id: roadmap_id
    }, (success) ->
      id = -1
      angular.forEach success, (member) ->
        if member.user.email == user.getUser().email
          id = member.id

      if id > -1
        members.delete {
          id: id
        },
          ->
            success_callback()
          ->
            failure_callback()

  @go = (roadmap_id, withoutReload) ->
    roadmap = findById(roadmap_id).roadmap

    if withoutReload
      magickURL 'r/' + roadmap.hash + '/' + urlName(roadmap.name)
    else
      $timeout ->
        $rootScope.$broadcast 'members:update', roadmap_id
      , 1000

      activity.clear()

      $location.path 'r/' + roadmap.hash + '/' + urlName(roadmap.name)

  @setPrivacy = (roadmap_id, privacy, callback) ->
    r = null
    angular.forEach roadmap_data, (roadmap) -> r = roadmap if roadmap.id == roadmap_id

    r.is_archive = false if typeof r.is_archive == 'undefined'

    if r
      r.is_private = privacy
      roadmaps.update {
        id: r.id
        name: r.name
        is_archive: r.is_archive
        is_private: r.is_private
      }, ->
        callback()

  @subscription = (roadmap_id, subscription) ->
    _.findWhere(roadmap_data, {id: roadmap_id}).is_subscribed = subscription

    if subscription
      subscriptions.save {roadmap_id: roadmap_id}
    else
      subscriptions.delete {roadmap_id: roadmap_id}

  @import = (file, callback) ->
    console.log '[Roadmap service] Import roadmap'

    $upload.upload({
      url: API_HOST + 'roadmaps/import'
      data: {
        token: user.getToken()
        file: file
      }
    }).success (response) ->
      fallback response.hash, callback

  @export = (roadmapHash) ->
    console.log '[Roadmap service] Export roadmap'

    roadmaps.export {hash: roadmapHash}, (success) ->
      exportingRoadmap = success
      delete exportingRoadmap['$promise']
      delete exportingRoadmap['$resolved']

      blob = new Blob [JSON.stringify(exportingRoadmap)], {type: "text/json;charset=utf-8"}
      saveAs blob, roadmapHash + '.json'

#  socket

  @changeNameFromSocket = (newName) ->
    if currentRoadmap
      $rootScope.$apply ->
        currentRoadmap.name = newName

  @changePrivacyFromSocket = (newPrivacy) ->
    if currentRoadmap
      $rootScope.$apply ->
        currentRoadmap.is_private = newPrivacy

  undefined
