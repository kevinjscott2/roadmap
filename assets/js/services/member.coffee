angular.module('roadmap').service 'member', ($rootScope, members) ->
  membersData = []
  roadmapId = null

  forceLoad = true

  @all = (roadmap, callback) ->
    if roadmapId != roadmap or forceLoad
      roadmapId = roadmap

      members.query {
        roadmap_id: roadmapId
      }, (success) ->
        forceLoad = false

        membersData = success
        callback membersData
    else
      callback membersData

  @add = (id, successCallback, failureCallback) ->
    forceLoad = true

    members.save {
      roadmap_id: roadmapId
      user_id: id
      rights: 1
    }, successCallback, failureCallback

  @revokeAccess = (id, callback) ->
    forceLoad = true

    members.delete {id: id}, callback

  @setPermissions = (member, rights, successCallback, failureCallback) ->
    members.update {
      id: member.id
      rights: rights
    }, successCallback, failureCallback

  @socketEvent = ->
    forceLoad = true

    $rootScope.$broadcast 'members:update'

  undefined
