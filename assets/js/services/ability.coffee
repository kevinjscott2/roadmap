SUPERADMIN = -1
ADMIN = 0
MEMBER = 1

angular.module('roadmap').service 'ability', (user) ->
  abilities = {}

  can = (ability, handler) ->
    if handler
      abilities[ability] = handler
    else
      abilities[ability] = -> true

  cannot = (ability, handler) ->
    if handler
      abilities[ability] = handler
    else
      abilities[ability] = -> false

  @ableTo = (ability, resource) ->
    if abilities[ability]
      abilities[ability](resource)
    else
      false

  @set = (role_id, admin) ->
    abilities = {}

    role_id = SUPERADMIN if admin

    switch role_id
      when SUPERADMIN
        console.log 'super admin'

        can 'roadmap:archive'
        can 'roadmap:manage'
        can 'roadmap:labels'

        can 'roadmap:import'
        can 'roadmap:export'

        can 'users:manage'

        cannot 'roadmap:leave'
      when ADMIN
        console.log 'admin'

        can 'roadmap:archive'
        can 'roadmap:manage'
        can 'roadmap:labels'
        can 'roadmap:leave', (roadmap) ->
          user.getUser().id == roadmap.owner_id

        can 'roadmap:import'
        can 'roadmap:export'
      when MEMBER
        console.log 'member'

        can 'roadmap:leave'

        cannot 'roadmap:labels'
        cannot 'roadmap:archive'
        cannot 'roadmap:manage'

        can 'roadmap:import'
        cannot 'roadmap:export'
      else
        cannot 'all'

  undefined
