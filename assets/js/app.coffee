app = angular.module('roadmap', ['ngRoute', 'ngAnimate', 'ngResource', 'ngCookies', 'ngDragDrop', 'infiniteScroll', 'keitoaino', 'hmTouchevents', 'ngRepeatReorder', 'ui.sortable', 'ui.date', 'angularMoment', 'angularFileUpload', 'monospaced.elastic'])

app.config ($routeProvider, $locationProvider) ->
  $routeProvider
    .when('/',
      templateUrl: 'dashboard.html',
      controller: 'dashboardCtrl'
    )
    .when('/r/:roadmap_hash',
      templateUrl: 'roadmap.html',
      controller: 'roadmapCtrl'
    )
    .when('/r/:roadmap_hash/:roadmap_name',
      templateUrl: 'roadmap.html',
      controller: 'roadmapCtrl'
      reloadOnSearch: false
    )
    .when('/m/:milestone_hash/:milestone_name',
      templateUrl: 'roadmap.html',
      controller: 'roadmapCtrl'
    )
    .when('/profile',
      templateUrl: 'profile.html',
      controller: 'profileCtrl'
    )
    .otherwise(
      redirectTo: '/'
    )
  $locationProvider.html5Mode true
