angular.module('roadmap').filter 'htmlSafe', ['$sce', ($sce)->
  (input)->
    $sce.trustAsHtml(input)
]