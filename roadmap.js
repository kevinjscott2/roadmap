var app, express, publicDir, viewsDir, server, io, redis, OAuth2;

OAuth2 = require('oauth').OAuth2;

express = require('express');
app = express();
server = app.listen(process.env.PORT || 3000);
io = require('socket.io').listen(server);
redis = require('redis');

publicDir = __dirname + '/public';
viewsDir = __dirname + '/views';

app.set('views', viewsDir);
app.set('view engine', 'jade');
app.use(require('connect-assets')());
app.use(express['static'](publicDir));

var clientId = null;
var secret = null;
var oauth = null;

if ((process.env.ENV == "production") || true) {
    clientId = "53f8f2493f5634ff0fdc";
    secret = "1a6f738c7c0cc3a0d5ee1aab85bdc12ed74e749d";
    oauth = new OAuth2(clientId, secret, "https://code.espn.com/", "login/oauth/authorize", "login/oauth/access_token");
}
else {
    clientId = "88ef9b56bfaaafa23aef";
    secret = "6ff3f75b3d2bb9e41590d028f063bdc2f3460e43";
    oauth = new OAuth2(clientId, secret, "https://github.com/", "login/oauth/authorize", "login/oauth/access_token");
}

app.get('/github_callback', function(req, res) {
    oauth.getOAuthAccessToken(req.query.code, {}, function (err, access_token) {
        return res.render('index', {token: access_token});
    });
});

app.get('/', function(req, res) { return res.render('index'); });
app.get('/:page.html', function(req, res) { return res.render(req.params.page); });
app.get('*', function(req, res) { return res.render('index'); });

io.sockets.on('connection', function (socket) {
  console.log('Client');
  var redisClient = redis.createClient();

  var currentRoadmapId;

  socket.on('action', function(data) {
     if (data.type == 'subscribe' && data.modelType == 'Roadmap' && data.modelId) {
         if (currentRoadmapId) {
             redisClient.unsubscribe('roadmap_' + currentRoadmapId);
         }

         currentRoadmapId = parseInt(data.modelId);
         redisClient.subscribe('roadmap_' + currentRoadmapId);
     }
  });

  // relay redis messages to connected socket
  redisClient.on('message', function(channel, message) {
    console.log("from rails to subscriber:", channel, message);
    socket.emit('message', message)
  });

  // unsubscribe from redis if session disconnects
  socket.on('disconnect', function () {
    console.log("user disconnected");

    redisClient.quit();
  });

});
